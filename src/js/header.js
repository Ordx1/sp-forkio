const hoverLogo = function () {
    const logoImg = document.querySelector('.logo__img');
    const logo = document.querySelector('.navbar__logo');
    logo.addEventListener("mouseover", () => logoImg.src="./dist/img/header/logo-hover.png");
    logo.addEventListener("mouseleave", () => logoImg.src="./dist/img/header/Logo.png");
}
   
hoverLogo();